import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { ChakraProvider, Button, Grid, GridItem, Center, Flex, ScaleFade, useToast, Heading, Text } from '@chakra-ui/react';
import styles from './App.less';
import { BASE_URL } from '../utils/constants';
import { getImageData } from '../service/api';


function Hello() {

  const toast = useToast();

  const [imageData, setImageData] = useState([]);
  const [idx, setIdx] = useState(0);

  const numberOfImagesPerFetch = 8;
  const date = new Date().toLocaleDateString();

  useEffect(() => {
    getImageData({
      format: 'js',
      idx,
      n: numberOfImagesPerFetch,
    }).then((res) => {
      console.log(res);
      const helperMap = new Map();
      const newImageData = imageData.concat(res?.images || []).filter(image=> !helperMap.has(image.startdate) && helperMap.set(image.startdate, ''))
      if (newImageData.length === imageData.length) {
        toast({
          title: '没有更多了',
          status: 'info',
          duration: 5000,
          isClosable: true,
          position: 'top',
        })
      }
      setImageData(newImageData);
    });
  }, [idx]);

  const backToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }

  const loadMore = () => {
    setIdx(idx + numberOfImagesPerFetch);
  };

  const todayImage = imageData?.[0] || {};

  return (
    <div className={styles.rootContainer}>
      <Flex direction="column" gap='1rem'>

        <ScaleFade initialScale={0.8} in>
          <Flex direction="column" gap='0.5rem'>
            <Heading>{date}</Heading>
            <Text fontSize='xl'>{`${todayImage?.title} - ${todayImage?.copyright}`}</Text>
            <div className={styles.imageBox}>
              <img className={styles.imageItem} src={`${BASE_URL}${todayImage?.url}`} alt="每日一图" />
            </div>
          </Flex>
        </ScaleFade>

        <Heading>历史</Heading>
        <Grid templateColumns='repeat(3, 1fr)' gap="1rem">
          {imageData.slice(1).map((imageItem) => (
            <ScaleFade key={imageItem.startdate} initialScale={0.8} in>
              <GridItem>
                <Flex direction="column" gap='0.5rem'>
                  <Text fontSize='xl'>{imageItem?.title}</Text>
                  <div className={styles.imageBox}>
                    <img className={styles.imageItem} src={`${BASE_URL}${imageItem.url}`} alt={imageItem.startdate} />
                  </div>
                </Flex>
              </GridItem>
            </ScaleFade>
          ))}
        </Grid>

        <Center>
          <Flex gap='2rem'>
            <Button colorScheme='blue' onClick={loadMore} isDisabled={false}>加载更多</Button>
            <Button colorScheme='blue' onClick={backToTop}>返回顶部</Button>
          </Flex>
        </Center>
      </Flex>
    </div>
  );
}

export default function App() {
  return (
    <ChakraProvider>
      <Router>
        <Routes>
          <Route path="/" element={<Hello />} />
        </Routes>
      </Router>
    </ChakraProvider>
  );
}
