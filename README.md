## Starting Development

Start the app in the `dev` environment:

```bash
npm i
npm start
```

## Packaging for Production

To package apps for the local platform:

```bash
npm run package
```
